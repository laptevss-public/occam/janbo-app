package main

import "testing"

func TestGreetingSpecific(t *testing.T) {
	greeting := CreateGreeting("John")
	if greeting != "Hello janbo from John [pod:unknown]\n" {
		t.Errorf("Greeting was incorrect, got: %s, want: %s.", greeting, "Hello, John [pod:unknown]\n")
	}
}

func TestGreetingDefault(t *testing.T) {
	greeting := CreateGreeting("")
	if greeting != "Hello janbo from Stas [pod:unknown]\n" {
		t.Errorf("Greeting was incorrect, got: %s, want: %s.", greeting, "Hello, Stas [pod:unknown]\n")
	}
}
