# Janbo https://janbo.z64x.com

## CI
- Hadolint: Dockerfile linter, validate inline bash
- Multi-stage build
- Run tests on build: go test
- Build app: go build, removing debug informations and compile only for linux target and disabling cross compilation
- Use scratch for runtime, docker image size: 4.6MB
- Use unprivileged user
- SAST (Static Application Security Testing): gosec, semgrep
- Secret detection: uses the Gitleaks tool to scan the repository for secrets
- Trivy: scanner for vulnerabilities in container images, file systems

## CD
- Run tests on deploy: helm hook
- Horizontal Pod Autoscaling
- securityContext: runAsNonRoot, readOnlyRootFilesystem, drop all capabilities
- automountServiceAccountToken set to false
- TLS: use Traefik to automatically obtain and renew certificates (I usually use either Traefik or Cert-Manager)
- Automatic deployment of new versions
- Application uninstall task

## Infrastructure and application
- Application: go was chosen due to the requirement for small size, I usually use bash in my work, rarely python
- Orchestrator: Kubernetes in Oracle cloud
- Ingress controller: Traefik

## Environment variables

- `JANBO_PORT` default `11130`

## TODO

- Add PodSecurityPolicy
- Redirect HTTP->HTTPS
