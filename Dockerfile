FROM golang:1.18.0-alpine3.15 as builder

# Add user, install git and ca certs
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
ENV USER=janbouser
ENV UID=88888
RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}" && \
    apk update && \
    apk add --no-cache git=2.34.1-r0

WORKDIR $GOPATH/src/janbo-package/janbo-app/
COPY src .

# Test and build app
RUN go mod download && \
    go mod verify && \
    CGO_ENABLED=0 go test -v && \
    ARCH=`uname -m` && \
    case "${ARCH}" in \
      aarch64) CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -ldflags="-w -s" -o /go/bin/janbo-app ;; \
      x86_64) CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/janbo-app ;; \
    esac

############################################################################

FROM scratch

ENV UID=88888

# Add certs and user
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Add app
COPY --from=builder /go/bin/janbo-app /go/bin/janbo-app

USER ${UID}:${UID}

EXPOSE 11130

ENTRYPOINT ["/go/bin/janbo-app"]